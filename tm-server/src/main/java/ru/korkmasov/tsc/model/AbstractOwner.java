package ru.korkmasov.tsc.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.entity.ITWBS;
import ru.korkmasov.tsc.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor

public abstract class AbstractOwner extends AbstractEntity implements ITWBS {

    private String userId;
    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date dateStart;

    @Nullable
    private String description;

    @NotNull
    private String name;

    @NotNull
    private Status status = Status.NOT_STARTED;

    public AbstractOwner(@NotNull final String name, @NotNull final String userId) {
        this.name = name;
        this.userId = userId;
    }

}


