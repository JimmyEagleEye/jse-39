package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.other.ISaltSettings;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService extends ISaltSettings {

    @NotNull String getApplicationVersion();

    int getBackupInterval();

    int getScannerInterval();

    @NotNull String getServerHost();

    int getServerPort();

    int getSessionCycle();

    @NotNull String getSessionSalt();

    @Nullable String getJdbcUser();

    @Nullable String getJdbcPassword();

    @Nullable String getJdbcUrl();

    @Nullable String getJdbcDriver();

}
