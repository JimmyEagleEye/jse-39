package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.api.repository.IOwnerRepository;
import ru.korkmasov.tsc.model.AbstractOwner;

public interface IOwnerService<E extends AbstractOwner> extends IService<E>, IOwnerRepository<E> {
}
