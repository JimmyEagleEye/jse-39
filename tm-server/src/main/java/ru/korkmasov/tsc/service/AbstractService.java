package ru.korkmasov.tsc.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.service.IService;
import ru.korkmasov.tsc.api.service.IConnectionService;
import ru.korkmasov.tsc.model.AbstractEntity;

import java.sql.Date;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    protected Date prepare(@Nullable final java.util.Date date) {
        if (date == null) return null;
        return new Date(date.getTime());
    }

}
