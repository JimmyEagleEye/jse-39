package ru.korkmasov.tsc.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.repository.IProjectRepository;
import ru.korkmasov.tsc.api.service.IConnectionService;
import ru.korkmasov.tsc.api.service.IProjectService;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.model.Project;

import java.sql.Connection;
import java.util.Date;
import java.util.Optional;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    @NotNull
    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public IProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            if (index > projectRepository.findAll(userId).size() - 1) throw new IndexIncorrectException();
            return projectRepository.findOneByIndex(userId, index);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            return projectRepository.findOneByName(userId, name);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            if (index > projectRepository.findAll(userId).size() - 1) throw new IndexIncorrectException();
            projectRepository.removeOneByIndex(userId, index);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.removeOneByName(userId, name);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById
            (@NotNull final String userId, @Nullable final String id,
             @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex
            (@NotNull final String userId, @Nullable final Integer index,
             @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findOneByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findOneByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findOneByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findOneByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findOneByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public Project add(@NotNull String userId, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            @Nullable final Project project = projectRepository.add(userId, name, description);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
}
